#include "gkEntity.h"
#include "gkGameObject.h"
#include "gkScene.h"
#include "Level.h"
#include "Loaders/Blender2/gkBlendFile.h"
#include "RedBloodCell.h"
#include <stdlib.h>
#include <time.h> 

#define RED_BLOODCELL_PHYSICS "RedBloodCell"

RedBloodCell::RedBloodCell(PlayableCharacter* player) {
	_player = player;
	srand((unsigned)time(NULL)); 
}

RedBloodCell::~RedBloodCell() {

}

RedBloodCell* RedBloodCell::clone(gkScene* dest) {
	RedBloodCell* redBloodCell = new RedBloodCell(_player);

	setPhysics(dest->cloneObject(dest->getObject(RED_BLOODCELL_PHYSICS), 0, true));

	return redBloodCell;
}

void RedBloodCell::follow(gkGameObject* object) {

}

void RedBloodCell::follow(Route* route) {

}

void RedBloodCell::setWaypoint(WayPoint* waypoint){
	_currentWaypoint = waypoint;
}

void RedBloodCell::load(gkBlendFile* file, gkScene* dest) {
	gkScene* tscene = file->getMainScene();

	if(tscene->hasObject(RED_BLOODCELL_PHYSICS))
	{
		setPhysics(tscene->getObject(RED_BLOODCELL_PHYSICS));
	}
	else
	{
		setPhysics(dest->cloneObject(dest->getObject(RED_BLOODCELL_PHYSICS), 0, true));
	}
	
	dest->addObject(getPhysics());
}

WayPoint* RedBloodCell::nextWaypoint(){

	vector <WayPoint*> tempvector = _currentWaypoint->next();

	int size = tempvector.size();

	int random = rand()%size;

	return tempvector[random];
}

void RedBloodCell::update(gkScalar delta) {

	if(getPhysics()->getPhysicsController()->collidesWith(_player->getPhysics())) {
			_player->setSpeed(2.f);
	}

	/*if(_currentWaypoint->getLocation()->getWorldPosition().distance(getPhysics()->getWorldPosition()) <= 2){
		_currentWaypoint = nextWaypoint();

		lookAt(_currentWaypoint->getLocation()->getWorldPosition());

		gkVector3 movement = getPhysics()->getOrientation() * gkVector3(0, 4.f, 0);
		getPhysics()->setLinearVelocity(movement);
	}else{
		lookAt(_currentWaypoint->getLocation()->getWorldPosition());

		gkVector3 movement = getPhysics()->getOrientation() * gkVector3(0, 4.f, 0);
		getPhysics()->setLinearVelocity(movement);
	}*/
	//if atdestination
	//set new destination
	//lookat destination
	//else nothing
}