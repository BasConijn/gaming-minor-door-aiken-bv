#pragma once

#include "MoveableCharacter.h"

class Input;

class PlayableCharacter : public MoveableCharacter {

public:
	PlayableCharacter();

	virtual ~PlayableCharacter();

	virtual void load(gkBlendFile* file, gkScene* dest) = 0;

	virtual void update(gkScalar delta)=0;

	virtual Input* getInput();
	virtual void setInput(Input* input);

	virtual gkCamera* getCamera();
	virtual void setCamera(gkCamera* camera);

	virtual gkGameObject* getXRot();
	virtual void setXRot(gkGameObject* xrot);

	virtual gkGameObject* getZRot();
	virtual void setZRot(gkGameObject* zrot);

private:
	gkGameObject* _xRot, *_zRot;
	gkCamera*     _camera;
	Input* _input;
	gkScalar _speed;
};


