#include "BaseMenu.h"

BaseMenu::BaseMenu(gkScene* scene) : _scene(scene), _next(0), _previous(0) {

} 


BaseMenu::~BaseMenu(void) {
}

void BaseMenu::loadMenu(){
	gkBlendFile* blend =  gkBlendLoader::getSingleton().loadFile(gkUtils::getFile("assets/models/MainMenuBackup.blend"), "", "Rocket");

	if (!blend)
	{
		gkPrintf("File loading failed.\n");
		return;
	}

	// Install fonts
	gkGUIManager *gm = gkGUIManager::getSingletonPtr();
	gm->loadFont("Delicious-Roman");
	gm->loadFont("Delicious-Bold");
	gm->loadFont("Delicious-Italic");
	gm->loadFont("Delicious-BoldItalic");

	// Initialise the Lua interface
	Rocket::Core::Lua::Interpreter::Initialise();
	Rocket::Controls::Lua::RegisterTypes(Rocket::Core::Lua::Interpreter::GetLuaState());

	// Enable debugger (shift+~)
	gm->setDebug(_scene->getDisplayWindow()->getGUI());
}

gkScene* BaseMenu::getScene() {
	return _scene;
}

BaseMenu* BaseMenu::getNext() {
	return _next;
}

void BaseMenu::setNext(BaseMenu* next) {
	_next = next;
}

BaseMenu* BaseMenu::getPrevious() {
	return _previous;
}

void BaseMenu::setPrevious(BaseMenu* previous) {
	_previous = previous;
}