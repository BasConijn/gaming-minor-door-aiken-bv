#define OGREKIT_COMPILE_LIBROCKET 1;

#include "Menu.h"
#include "Application.h"
#include "HighscoreMenu.h"
#include "GameOverMenu.h"


Menu::Menu(gkScene* scene) : BaseMenu(scene), _document(0) {
	string buttons[] = { "Play", "Highscore", "Settings", "Quit" };
	
	for(int i =0; i < buttons->size(); i++) {
		_buttons.push_back( pair<string, Rocket::Core::Element*>(buttons[i], (Rocket::Core::Element*)0) );
	}
}

Menu::~Menu() {
	unloadMenu();
}

void Menu::loadMenu() {
		
	BaseMenu::loadMenu();

	// Create context
	gkGUI *gui = getScene()->getDisplayWindow()->getGUI();
	
	// Load the mouse cursor and release the caller's reference.
	_cursor = gui->getContext()->LoadMouseCursor("Cursor.rml");
	if (_cursor)
		_cursor->RemoveReference();

	_document = gui->getContext()->LoadDocument("MainMenu.rml");
	if (_document) {
		for(int i = 0; i < _buttons.size(); i++) {
			_buttons[i].second = _document->GetElementById(_buttons[i].first.c_str());
			_buttons[i].second->AddEventListener("click", this);
		}
		_document->Show();		
	}
}

void Menu::ProcessEvent(Rocket::Core::Event& event) {
		
	Rocket::Core::String name = event.GetTargetElement()->GetId();

	switch(_buttonToEnum(name)) {
		case Play:
			Application::getSingleton()->resume();
			break;
		case Highscore: {
			unloadMenu();
			setNext(new HighscoreMenu(getScene()));
			getNext()->setPrevious(this);
			getNext()->loadMenu();
			break;
		}

		case Settings: {
			break;
		}
		case Quit:
			Application::getSingleton()->quit();
			break;
	}
}

void Menu::unloadMenu(){
	if(_document){
		getScene()->getDisplayWindow()->getGUI()->getContext()->UnloadAllMouseCursors();
		getScene()->getDisplayWindow()->getGUI()->getContext()->UnloadAllDocuments();
			
		_document->RemoveReference();
		_document = 0;

		Rocket::Core::Lua::Interpreter::Shutdown();

		for(int i = 0; i < _buttons.size(); i++) {
			_buttons[i].second->RemoveEventListener("click", this);
		}
	}
} 

int Menu::_buttonToEnum(Rocket::Core::String name) {
	for(int i = 0; i < _buttons.size(); i++) {
		if(Rocket::Core::String(_buttons[i].first.c_str()) == name) {
			return i;
		}
	}
	return -1;
}