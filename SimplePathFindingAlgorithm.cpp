#include "SimplePathFindingAlgorithm.h"
#include "WayPoint.h"
#include <vector>
#include "gkLogger.h"

SimplePathFindingAlgorithm::SimplePathFindingAlgorithm() {

}

SimplePathFindingAlgorithm::~SimplePathFindingAlgorithm() {

}

void SimplePathFindingAlgorithm::calculatePath(WayPoint* current, WayPoint* dest, stack<WayPoint*>& route) {

	while(!route.empty()) {
		route.pop();
	}

	WayPoint* temp = dest; 
	while(temp != current) 	{
		route.push(temp);
		temp = temp->previous()[0];
	}
	route.push(temp);

	gkLogMessage("Calculation -> Path next is " << route.top()->getName() << " Destination: " << dest->getName());
}
