#pragma once
#include "basemenu.h"

class ScoreCalculator;

class GameOverMenu :
	public BaseMenu
{
public:
	GameOverMenu(gkScene* scene, ScoreCalculator* scoreCalculator);

	virtual ~GameOverMenu(void);

	virtual void loadMenu();

	virtual void unloadMenu();

	virtual void ProcessEvent(Rocket::Core::Event& event);

private:
	Rocket::Core::ElementDocument* _document;
	Rocket::Core::ElementDocument* _cursor;
	Rocket::Core::Element* _button;

	ScoreCalculator* _scoreCalculator;
};



