#pragma once

class Input {

public:

	enum InputCode {
		IC_FORWARD,
		IC_BACKWARD,
		IC_LEFT,
		IC_RIGHT,
		IC_BUTTON_0,
		IC_BUTTON_1,
		IC_BUTTON_2,
		IC_STOP,
		IC_MAX
	};

public:

	virtual ~Input() { };

	virtual bool hasInput(const InputCode& ic) = 0;

	virtual void moveCamera(void) = 0;
	virtual void movePlayer(void) = 0;

	virtual void bindScanCode(const InputCode& ic, const int &code)=0;
};