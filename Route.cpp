#include "Route.h"
#include "WayPointManager.h"
#include "WayPoint.h"
#include "gkGameObject.h"
#include "PathFindingAlgorithm.h"
#include "gkLogger.h"

#define STOPDISTANCE 2.5

Route::Route(PathFindingAlgorithm* algorithm) : _algorithm(algorithm), _currentWayPoint(0), _destination(0), _destinationWayPoint(0) {
	
}

Route::~Route() {
	
}

void Route::setDestination(gkGameObject* destination) {
	_destination = destination;
	_destinationWayPoint = WayPointManager::getSingleton()->getClosestWayPoint(destination);
}

gkGameObject* Route::getDestination() {
	return _destination;
}

bool Route::isAtDestination(gkGameObject* currentPosition) {
	if(!_destinationWayPoint)
		throw "Route -> DestinationWaypoint has not been set";

	bool isAtDestination = _destinationWayPoint->getLocation()->getWorldPosition().distance(currentPosition->getWorldPosition()) <= STOPDISTANCE;

	if(isAtDestination) {
		gkLogMessage("Route -> isAtDestination = true");
	}

	return isAtDestination;
}

WayPoint* Route::_getClosestWayPointFromCurrentWayPoint(WayPoint* currentWayPoint, gkGameObject* currentLocation) {
	WayPoint* closestWayPoint = 0;
	double closestDistance;

	vector<WayPoint*> waypoints;
	vector<WayPoint*> next = currentWayPoint->next();
	vector<WayPoint*> prev = currentWayPoint->previous();
	waypoints.push_back(currentWayPoint);
	waypoints.insert(waypoints.end(), next.begin(), next.end());
	waypoints.insert(waypoints.end(), prev.begin(), prev.end());

	vector<WayPoint*>::iterator iterator = waypoints.begin();
	for(; iterator != waypoints.end(); iterator++) {
		WayPoint* wayPoint = (*iterator);
		double distance = wayPoint->getLocation()->getWorldPosition().squaredDistance(currentLocation->getWorldPosition());
		if(closestWayPoint == 0 || distance < closestDistance) {
			closestWayPoint = wayPoint;
			closestDistance = distance;
		}
	}

	//gkLogMessage("Closest Waypoint: " << closestWayPoint->getName() << " squaredDistance: " << closestDistance);

	return closestWayPoint;
}

bool Route::isDestinationWayPointChanged(WayPoint* newDestinationWayPoint) {
	WayPoint* closestWayPoint = _getClosestWayPointFromCurrentWayPoint(_destinationWayPoint, _destination);

	bool isDestinationChanged = closestWayPoint != _destinationWayPoint;

	if(isDestinationChanged) {
		gkLogMessage("Route -> isDestinationChanged = true. New destinationWayPoint is " << closestWayPoint->getName() );
		_destinationWayPoint = closestWayPoint;
	}

	return isDestinationChanged;
}

gkGameObject* Route::getHeading(gkGameObject* currentPosition) {
	if(!_currentWayPoint) {
		_currentWayPoint = WayPointManager::getSingleton()->getClosestWayPoint(currentPosition);
	}

	//if the destination waypoint is not changed, also don't change the route
	if(_route.empty() || isDestinationWayPointChanged(_destinationWayPoint)) {
		_algorithm->calculatePath(_currentWayPoint, _destinationWayPoint, _route); //calculate the route
	}

	//set next waypoint to the next in the route
	if(_currentWayPoint->getLocation()->getWorldPosition().distance(currentPosition->getWorldPosition()) <= STOPDISTANCE) {
		WayPoint* previousWayPoint = _route.top();
		_route.pop();
		
		_currentWayPoint = _route.top();
		gkLogMessage("Route -> I am at wayPoint " << previousWayPoint->getName() << " going to " << _currentWayPoint->getName());
	}

	//return the gameobject
	return _currentWayPoint->getLocation();
}
