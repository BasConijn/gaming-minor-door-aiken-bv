#include "StringHelper.h"

void StringHelper::split(vector<string> &tokens, const string &text, string delimiter) {
	string s(text.begin(), text.end());

	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		tokens.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	tokens.push_back(s);
}
