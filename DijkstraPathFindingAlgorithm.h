#pragma once

#include "PathFindingAlgorithm.h"

class WayPoint;

class DijkstraPathFindingAlgorithm : public PathFindingAlgorithm {

public:
	DijkstraPathFindingAlgorithm();
	virtual ~DijkstraPathFindingAlgorithm();
	virtual void calculatePath(WayPoint* current, WayPoint* dest, stack<WayPoint*>& route);
};

