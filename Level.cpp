#include "Application.h"
#include "DijkstraPathFindingAlgorithm.h"
#include "gkGameObject.h"
#include "gkGameObjectManager.h"
#include "gkGroupManager.h"
#include "gkFontManager.h"
#include "gkLogger.h"
#include "KeyboardAndMouseInput.h"
#include "Level.h"
#include "OgreKit.h"
#include "RedBloodCell.h"
#include "Route.h"
#include "SimplePathFindingAlgorithm.h"
#include "ui/Menu.h"
#include "Virus.h"
#include "WayPoint.h"
#include "WayPointManager.h"
#include "WhiteBloodCell.h"

//Sort http://www.textfixer.com/tools/alphabetical-order.php

#define LEVEL_GROUP_NAME "Game"
#define LEVEL_BLENDER_FILE "assets/models/Level.blend"
#define VIRUS_BLENDER_FILE "assets/models/ProtoVirus.blend"
#define RED_BLOODCELL_BLENDER_FILE "assets/models/RedBloodCell.blend"
#define WHITE_BLOODCELL_BLENDER_FILE "assets/models/WhiteBloodCell.blend"

Level::Level() : _scene(0) {
	gkGameObjectManager::getSingleton().addInstanceListener(this);
    gkGameObjectManager::getSingleton().addResourceListener(this);
    gkSceneManager::getSingleton().addInstanceListener(this);
    gkSceneManager::getSingleton().addResourceListener(this);
    gkGroupManager::getSingleton().addResourceListener(this);
    gkTextManager::getSingleton().addResourceListener(this);
    gkFontManager::getSingleton().addResourceListener(this);
}

Level::~Level()  {
	gkGameObjectManager::getSingleton().removeResourceListener(this);
    gkGameObjectManager::getSingleton().removeInstanceListener(this);
    gkSceneManager::getSingleton().removeInstanceListener(this);
    gkSceneManager::getSingleton().removeResourceListener(this);
    gkGroupManager::getSingleton().removeResourceListener(this);
    gkTextManager::getSingleton().removeResourceListener(this);
    gkFontManager::getSingleton().removeResourceListener(this);

	delete _player;
	
	vector<NPC*>::iterator enemyIterator = _enemies.begin();
	for(; enemyIterator !=  _enemies.end(); enemyIterator++) {
		delete (*enemyIterator);
	}

	if(_scene)
		gkSceneManager::getSingleton().destroy(_scene->getResourceHandle());
}

void Level::loadLevel() {


		gkBlendFile* virusData = gkBlendLoader::getSingleton().loadFile(gkUtils::getFile(VIRUS_BLENDER_FILE), "ProtoVirusScene", LEVEL_GROUP_NAME);

        if (!virusData || !virusData->getMainScene())
        {
                gkPrintf("Level: Blend '%s' loading failed", VIRUS_BLENDER_FILE);
                return;
        }

		gkPrintf("Level: Blend '%s' loaded", VIRUS_BLENDER_FILE);

        gkBlendFile* redBloodCellData = gkBlendLoader::getSingleton().loadFile(gkUtils::getFile(RED_BLOODCELL_BLENDER_FILE), "RedBloodCellScene", LEVEL_GROUP_NAME);

        if (!redBloodCellData || !redBloodCellData->getMainScene())
        {
                gkPrintf("Level: Blend '%s' loading failed", RED_BLOODCELL_BLENDER_FILE);
                return;
        }

         //log status
        gkPrintf("Level: Blend '%s' loaded", RED_BLOODCELL_BLENDER_FILE);

		gkBlendFile* whiteBloodCellData = gkBlendLoader::getSingleton().loadFile(gkUtils::getFile(WHITE_BLOODCELL_BLENDER_FILE), "WhiteBloodCellScene", LEVEL_GROUP_NAME);

        if (!whiteBloodCellData || !whiteBloodCellData->getMainScene())
        {
                gkPrintf("Level: Blend '%s' loading failed", WHITE_BLOODCELL_BLENDER_FILE);
                return;
        }

        //log status
        gkPrintf("Level: Blend '%s' loaded", WHITE_BLOODCELL_BLENDER_FILE);

        gkBlendFile* mapData = gkBlendLoader::getSingleton().loadFile(gkUtils::getFile(LEVEL_BLENDER_FILE), "LevelScene", LEVEL_GROUP_NAME);

        if (!mapData || !mapData->getMainScene())
        {
                gkPrintf("Level: Blend '%s'->Level loading failed", LEVEL_BLENDER_FILE);
                return;
        }

        // log status
        gkPrintf("Level: Blend '%s'->Level loaded", LEVEL_BLENDER_FILE);

        _scene = mapData->getMainScene();

        _scene->setHorizonColor(gkColor(0.2f, 0.2f, 0.2f));
        _scene->setAmbientColor(gkColor(0.2f, 0.2f, 0.2f));
		_scene->setGravity(gkVector3(0,0,0));

		_player = new Virus();
		_player->setInput(new KeyboardAndMouseInput(_player));
		_player->load(virusData, _scene);

		_scene->createInstance();

        WayPointManager* wayPointManager = WayPointManager::getSingleton();
        wayPointManager->load(mapData, _scene);

        WayPoint* spawnWayPoint = wayPointManager->getWayPointByName("Cube.002");
        _player->getPhysics()->setPosition(spawnWayPoint->getLocation()->getWorldPosition());
        _player->lookAt(spawnWayPoint->next()[0]->getLocation()->getWorldPosition());
		
		spawnRedBloodCells(redBloodCellData);

        PathFindingAlgorithm* algo = new DijkstraPathFindingAlgorithm();
        Route* route = new Route(algo);
        route->setDestination(_player->getPhysics());

        WhiteBloodCell* whiteBloodCell = new WhiteBloodCell();
        whiteBloodCell->load(whiteBloodCellData, _scene);
        whiteBloodCell->follow(route);
        whiteBloodCell->getPhysics()->setPosition(wayPointManager->getWayPointByName("Cube")->getLocation()->getWorldPosition());
        _enemies.push_back(whiteBloodCell);

}

gkScene* Level::getScene() const {
	return _scene;
}

void Level::notifyInstanceCreated(gkInstancedObject* inst) {
	gkLogMessage("Level: Instanced -> " << inst->getResourceName().getName());
}

void Level::notifyResourceCreated(gkResource* res) {
	 gkLogMessage(res->getManagerType() << ", " << res->getResourceType() <<
        ":handle " <<  res->getResourceHandle() << ", created " << res->getResourceName().getName());
}

void Level::update(gkScalar delta) {
	_player->update(delta);

	vector<NPC*>::iterator iterator = _enemies.begin();
	for(; iterator!=_enemies.end(); iterator++)
	{
		(*iterator)->update(delta);
	}

	 WayPointManager::getSingleton()->update(delta);
}

void Level::resume() {
	_player->resume();
	
	vector<NPC*>::iterator enemyIterator = _enemies.begin();
	for(; enemyIterator !=  _enemies.end(); enemyIterator++) {
		(*enemyIterator)->resume();
	}
}

void Level::pause() {
	_player->pause();
	
	vector<NPC*>::iterator enemyIterator = _enemies.begin();
	for(; enemyIterator !=  _enemies.end(); enemyIterator++) {
		(*enemyIterator)->pause();
	}
}

void Level::spawnRedBloodCells(gkBlendFile* redBloodCellData) {
	set<WayPoint*> allWayPoints = WayPointManager::getSingleton()->getAllWayPoints();
	set<WayPoint*>::iterator iterator = allWayPoints.begin();
	for(; iterator != allWayPoints.end(); iterator++) 	{
		RedBloodCell* redBloodCell = new RedBloodCell(_player);
		redBloodCell->load(redBloodCellData, _scene);

		gkLogMessage("World Pos" << (*iterator)->getLocation()->getWorldPosition());
		redBloodCell->getPhysics()->setPosition((*iterator)->getLocation()->getWorldPosition());

		redBloodCell->setWaypoint((*iterator));
		//redBloodCell->setWaypoint( redBloodCell->nextWaypoint() ); 

		gkScalar randX = (rand()%2) - 1;
		gkScalar randY = (rand()%2) - 1;
		gkScalar randZ = (rand()%2) - 1;

		gkVector3 direction(randX, randY, randZ);

		_enemies.push_back(redBloodCell);
	}
}