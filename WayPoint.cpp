#include "WayPoint.h"
#include "OgreKit.h"

WayPoint::WayPoint(string name, gkGameObject* location) :_name(name), _location(location) {
	
}

WayPoint::~WayPoint() {

}

gkGameObject* WayPoint::getLocation() {
	return _location;
}

string WayPoint::getName() {
	return _name;
}

vector<WayPoint*> WayPoint::next() {
	vector<WayPoint*> wayPoints;

	map<WayPoint*, double>::iterator iterator = _next.begin();

	for(; iterator!= _next.end(); iterator++) {
		wayPoints.push_back((iterator)->first);
	}

	return wayPoints;
}

vector<WayPoint*> WayPoint::previous() {
	vector<WayPoint*> wayPoints;

	map<WayPoint*, double>::iterator iterator = _previous.begin();

	for(; iterator!= _previous.end(); iterator++) {
		wayPoints.push_back((iterator)->first);
	}

	return wayPoints;
}

void WayPoint::addNext(WayPoint* waypoint) {
	double distance = _location->getPosition().squaredDistance(waypoint->getLocation()->getPosition());
	_next[waypoint] = distance;
}

void WayPoint::addPrevious(WayPoint* waypoint) {
	double distance = _location->getPosition().squaredDistance(waypoint->getLocation()->getPosition());
	_previous[waypoint] = distance;
}

void WayPoint::update(gkScalar delta) {
	
}

string WayPoint::constructName(string baseName, int number) {
	if(number == 0)
		return baseName;

	stringstream ss;
	ss << number;
	string numberString = ss.str();

	while(numberString.size() < 3)
	{
		numberString = '0' + numberString;
	}

	return baseName + "." + numberString;
}