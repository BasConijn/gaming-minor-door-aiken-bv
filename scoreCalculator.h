#pragma once

#include "OgreTimer.h"

class ScoreCalculator
{
public:
	ScoreCalculator(void);
	~ScoreCalculator(void);

	virtual void start();
	virtual void pause();
	virtual long calculateScore();

private:
	Ogre::Timer* timer;
	long _timeBeforePauze;
	long _startTime;
};

