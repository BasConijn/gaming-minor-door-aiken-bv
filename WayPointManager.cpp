#include "OgreKit.h"
#include "WayPointManager.h"
#include "WayPoint.h"
#include <iostream>
#include <fstream>
#include "gkLogger.h"
#include <stdio.h>
#include "StringHelper.h"

#define WAYPOINTFILE "assets/Waypointfile.txt"

WayPointManager::WayPointManager() {

}

WayPointManager::~WayPointManager() {
	map<string, WayPoint*>::iterator iterator = waypoints.begin();
	for(; iterator != waypoints.end(); iterator++) 	{
		delete (iterator)->second;
	}
}

WayPointManager* WayPointManager::getSingleton() {
	static WayPointManager* instance = new WayPointManager();

	return instance;
}

void WayPointManager::load(gkBlendFile* file, gkScene* dest) {
	gkScene* tscene = file->getMainScene();

	ifstream wayPointFile(WAYPOINTFILE);

	if(!wayPointFile.is_open())
		return;

	string line, baseName;
	int lineNumber = 0;
	while (!wayPointFile.eof()) {
		getline(wayPointFile,line);

		if(lineNumber == 0) {
			unsigned pos(line.find("Name: "));
			line.replace(pos, pos + 6, "");
			baseName = line;
		}
		else if(line != "") {
			vector<string> parts;
			StringHelper::split(parts, line, "->");

			//Replace ".." with numbers that are in between
			vector<string>::iterator iterator = parts.begin();
			for(; iterator != parts.end(); iterator++) {
				if(*iterator == "..") {
					int from = atoi((*(iterator-1)).c_str());
					int to = atoi((*(iterator+1)).c_str());
					int delta = to - from;

					int increaser = 1; //can also be used to negative paths
					if(delta < 0) {
						increaser = -1;
					}

					iterator = parts.erase(iterator);
					iterator--;

					for(int j = 0; j != delta - increaser; j += increaser) {
						stringstream ss;
						ss << j + from + increaser;
						iterator = parts.insert(iterator + 1, ss.str()); 
					}
				}
			}

			//Parse number and create waypoints
			for(int i = 0; i < parts.size() - 1; i++) {
				int from = atoi(parts[i].c_str());
				int to = atoi(parts[i+1].c_str());
				
				WayPoint* fromWayPoint = _getWayPointAndCreateIfNotExists( WayPoint::constructName(baseName, from), tscene);
				WayPoint* toWayPoint = _getWayPointAndCreateIfNotExists( WayPoint::constructName(baseName, to), tscene);
				fromWayPoint->addNext(toWayPoint);
				toWayPoint->addPrevious(fromWayPoint);
			}
		}
		lineNumber++;
	}
}

void WayPointManager::update(gkScalar delta) {
	map<string, WayPoint*>::iterator iterator = waypoints.begin();
	for(; iterator != waypoints.end(); iterator++)
	{
		(iterator)->second->update(delta);
	}
}

WayPoint* WayPointManager::getClosestWayPoint(gkGameObject* object) {
	WayPoint* closestWayPoint = 0;
	double closestDistance;

	map<string, WayPoint*>::iterator iterator = waypoints.begin();
	for(; iterator != waypoints.end(); iterator++) {
		WayPoint* waypoint = (iterator)->second;
		double distance = waypoint->getLocation()->getWorldPosition().squaredDistance(object->getWorldPosition());
		if(closestWayPoint == 0 || distance < closestDistance ) {
			closestWayPoint = waypoint;
			closestDistance = distance;
		}
	}

	gkLogMessage("WayPointManager -> Closest Waypoint to " << object->getPosition() << " is " << closestWayPoint->getName() << " at " << closestWayPoint->getLocation()->getPosition() );

	return closestWayPoint;
}

WayPoint* WayPointManager::getWayPointByName(string name) {
	return waypoints[name];
}

WayPoint* WayPointManager::_getWayPointAndCreateIfNotExists(string name, gkScene* scene) {
	if(waypoints.find(name) == waypoints.end())
	{
		if(!scene->hasObject(name)) {
			gkPrint("No object has been found with " + name);
			throw "No object has been found with " + name;
		}

		gkGameObject* location = scene->getObject(name);
		WayPoint* wayPoint = new WayPoint(name, location);
		waypoints[name] = wayPoint;
	}
	return waypoints[name];
}

set<WayPoint*> WayPointManager::getAllWayPoints() {

	set<WayPoint*> tempWayPoints;
	map<string, WayPoint*>::iterator iterator = waypoints.begin();
	for(; iterator != waypoints.end(); iterator++) 	{
		tempWayPoints.insert((iterator)->second);
	}

	return tempWayPoints;
}