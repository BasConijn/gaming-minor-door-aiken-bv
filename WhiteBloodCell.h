#pragma once

#include "NPC.h"

class Route;
class gkGameObject;

class WhiteBloodCell : public NPC {

public:
	WhiteBloodCell();
	virtual ~WhiteBloodCell();

	virtual WhiteBloodCell* clone(WhiteBloodCell* original);

	virtual void follow(gkGameObject* object);

	virtual void follow(Route* route);

	virtual void load(gkBlendFile* file, gkScene* dest);

	virtual void update(gkScalar delta);
	
	enum States {
		FollowRoute = 0,
		LineOfSightChasing = 1
	};

private:
	Route* _route;
	gkGameObject* _objectToFollow;

	void followRoute();
	void lineOfSightChase();
};

