#pragma once

#include "gkInstancedManager.h"
#include "gkResourceManager.h"
#include "OgreKit.h"

using namespace std;

class NPC;
class Path;
class PlayableCharacter;

class Level : 
	  public gkInstancedManager::InstancedListener,
	  public gkResourceManager::ResourceListener
{

public:
	Level();
	virtual ~Level();

	virtual void loadLevel();

	virtual gkScene* getScene() const;

	virtual void update(gkScalar delta);

	virtual void pause();

	virtual void resume();

	virtual void spawnRedBloodCells(gkBlendFile* redBloodCellData);

protected:
	virtual void notifyInstanceCreated(gkInstancedObject* inst);
	virtual void notifyResourceCreated(gkResource* res);

private:
	gkScene* _scene;
	PlayableCharacter* _player;
	vector<NPC*> _enemies;
};

