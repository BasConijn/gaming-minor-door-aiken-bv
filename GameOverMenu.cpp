#include "GameOverMenu.h"
#include "HighScore.h"
#include "HighScoreElement.h"
#include "ScoreCalculator.h"

GameOverMenu::GameOverMenu(gkScene* scene, ScoreCalculator* scoreCalculator) : BaseMenu(scene), _scoreCalculator(scoreCalculator), _document(0)
{
}


GameOverMenu::~GameOverMenu(void)
{
	unloadMenu();
}

void GameOverMenu::loadMenu(){
	BaseMenu::loadMenu();

	gkGUI *gui = getScene()->getDisplayWindow()->getGUI();
	
	// Load the mouse cursor and release the caller's reference.
	_cursor = gui->getContext()->LoadMouseCursor("Cursor.rml");
	if (_cursor)
		_cursor->RemoveReference();

	_document = gui->getContext()->LoadDocument("GameOver.rml");

	if (_document) {
		
		_button = _document->GetElementById("Submit");
		_button->AddEventListener("click", this);

		_document->Show();		
	}
}

void GameOverMenu::unloadMenu(){
		getScene()->getDisplayWindow()->getGUI()->getContext()->UnloadAllMouseCursors();
		getScene()->getDisplayWindow()->getGUI()->getContext()->UnloadAllDocuments();
			
		_document->RemoveReference();
		_document = 0;

		Rocket::Core::Lua::Interpreter::Shutdown();

		_button->RemoveEventListener("click", this);
}

void GameOverMenu::ProcessEvent(Rocket::Core::Event& event) {
		Rocket::Core::Element* elm = _document->GetElementById("NameInput");
		HighScoreElement currentScore(static_cast<Rocket::Controls::ElementFormControl*>(elm)->GetValue().CString(), _scoreCalculator->calculateScore());
		HighScore* highScoreList = HighScore::getSingleton();
		highScoreList->readHighScoreFromFile();
		highScoreList->addHighScore(currentScore);
		highScoreList->writeHighScoreToFile();

		unloadMenu();
		getPrevious()->loadMenu();
}