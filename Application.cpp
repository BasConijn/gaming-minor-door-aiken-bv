#pragma once

#include "gkLogger.h"
#include "gkMemoryTest.h"
#include "gkUserDefs.h"
#include "Level.h"
#include "Application.h"
#include <sstream>
#include "Menu.h"
#include "gkLogger.h"
#include "GameOverMenu.h"

#define GK_CPP_VERBOSE_LOG false

Application::Application(): _paused(false) {
	_level = new Level();
	_scoreCalculator = new ScoreCalculator();

	_keyboard = gkWindowSystem::getSingleton().getKeyboard();

	_level->loadLevel();
	_scoreCalculator->start();
	loadHuds();

	_menu = new Menu(_level->getScene());
	pause();
}
	
Application::~Application(){
	delete _scoreCalculator;
	delete _menu;
	delete _level;
}

void Application::resume() {
	if(_paused){
		_paused = false;
		_scoreCalculator->start();
		_menu->unloadMenu();
		_level->resume();
	}
}

void Application::quit(){

	//HighScoreElement currentScore("Name", _scoreCalculator->calculateScore());
	//HighScore* highScoreList = HighScore::instance();
	//highScoreList->readHighScoreFromFile();
	//highScoreList->addHighScore(currentScore);
	//highScoreList->writeHighScoreToFile();
	////exit game
	gkEngine::getSingleton().requestExit();
}

void Application::gameOver(){
	pause();
	_menu->unloadMenu();
	_menu->setNext(new GameOverMenu(_level->getScene(), _scoreCalculator));
	_menu->getNext()->setPrevious(_menu);
	_menu->getNext()->loadMenu();

	//go to main menu
	//TODO: Go to the main menu
	//gkEngine::getSingleton().requestExit();
}

void Application::pause(){
	//pauze game
	if(!_paused){
		_paused =true;
		_scoreCalculator->pause();
		_menu->loadMenu();
		_level->pause();
	}
}

bool Application::isPaused() {
	return _paused;
}

void Application::loadHuds(){

	_hudScore = gkHUDManager::getSingleton().getOrCreate("_hudScore");
	if (_hudScore)
	{
		_score = _hudScore->getChild("_hudScore/_score");
		_hudScore->show();
	}
}

void Application::tick(gkScalar delta) {
	
	if(!_paused) {
		_level->update(delta);

		std::stringstream ss;
		ss << "Time: ";
		ss << _scoreCalculator->calculateScore();

		if(_score)
		_score->setValue(ss.str());
	}

	if (_keyboard->key_count > 0)
	{
		if (_keyboard->isKeyDown(KC_ESCKEY))
			if(!_paused){
				pause();
			}
	}
}

Application* Application::getSingleton() {
	static Application* instance = new Application();

	return instance;
}