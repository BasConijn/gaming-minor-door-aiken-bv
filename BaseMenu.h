#pragma once

#define OGREKIT_COMPILE_LIBROCKET 1;

#include "OgreKit.h"

#include "GUI/gkGUI.h"
#include "GUI/gkGUIManager.h"
#include "Rocket/Controls.h"
#include "Rocket/Core/Lua/Interpreter.h"
#include "Rocket/Controls/Lua/Controls.h"

#include "gkScene.h"

using namespace std;

class BaseMenu :
	public Rocket::Core::EventListener
{
public:
	BaseMenu(gkScene* scene);

	virtual ~BaseMenu(void);

	virtual void loadMenu();

	virtual void unloadMenu() = 0;

	virtual void ProcessEvent(Rocket::Core::Event& event) = 0;

	virtual gkScene* getScene();

	virtual BaseMenu* getNext();
	virtual void setNext(BaseMenu* next);

	virtual BaseMenu* getPrevious();
	virtual void setPrevious(BaseMenu* previous);

private:
	gkScene* _scene;
	BaseMenu* _next;
	BaseMenu* _previous;
};

