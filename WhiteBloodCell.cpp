#include "OgreKit.h"
#include "Level.h"
#include "WhiteBloodCell.h"
#include "Route.h"
#include "WayPointManager.h" 
#include "WayPoint.h" 
#include "Application.h"

#define WHITE_BLOODCELL_PHYSICS "WhiteBloodCell"

WhiteBloodCell::WhiteBloodCell() : _route(0), _objectToFollow(0) {
	setSpeed(12.f);
}

WhiteBloodCell::~WhiteBloodCell() {
	if(_route) {
		delete _route;
	}
}

WhiteBloodCell* WhiteBloodCell::clone(WhiteBloodCell* original) {
	return original;
}

void WhiteBloodCell::follow(gkGameObject* object) {
	_objectToFollow = object;
}

void WhiteBloodCell::follow(Route* route) {
	_route = route;
}

void WhiteBloodCell::load(gkBlendFile* file, gkScene* dest) {
	gkScene* tscene = file->getMainScene();
	
	if(tscene->hasObject(WHITE_BLOODCELL_PHYSICS))
	{
		setPhysics(tscene->getObject(WHITE_BLOODCELL_PHYSICS));
	}
	else
	{
		setPhysics(dest->cloneObject(dest->getObject(WHITE_BLOODCELL_PHYSICS), 0, true));
	}
	
	dest->addObject(getPhysics());

	setState(States::FollowRoute, false);
}

void WhiteBloodCell::update(gkScalar delta) { 
	gkFSM::update();

	switch(getState()) {
		case FollowRoute:
			followRoute();
			break;
		case LineOfSightChasing:
			lineOfSightChase();
			break;
	}
}

void WhiteBloodCell::followRoute() {
	
	if(!_route) return;

	if(!_route->isAtDestination(getPhysics())) {
		gkGameObject* next = _route->getHeading(getPhysics());

		lookAt(next->getWorldPosition());

		gkVector3 movement = getPhysics()->getOrientation()  * gkVector3(0, getSpeed(), 0);
		getPhysics()->setLinearVelocity(movement);
	}
	else {
		gkLogMessage("At destination " << getPhysics()->getWorldPosition() << " going to line of sight mode" );
		setState(LineOfSightChasing);
	}
}

void WhiteBloodCell::lineOfSightChase() {
	if(!_route) return;

	lookAt(_route->getDestination()->getWorldPosition());

	gkVector3 movement = getPhysics()->getOrientation()  * gkVector3(0, getSpeed(), 0);
	getPhysics()->setLinearVelocity(movement);

	gkRayTest rayTest;
	notMeFilter test(_route->getDestination());
	//gkLogMessage("Origin: " << ray.getOrigin() << " Direction " << ray.getDirection());
	
	if(rayTest.collides(getPhysics()->getWorldPosition(), _route->getDestination()->getWorldPosition(), test))
	{
		gkLogMessage("Not line of sight going to following route because collided with " << rayTest.getObject()->getName() );
		setState(FollowRoute);
	}

	if(getPhysics()->getPhysicsController()->collidesWith(_route->getDestination())) {
		Application::getSingleton()->gameOver();
	}
}