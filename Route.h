#pragma once

#include <stack>

using namespace std;

class WayPoint;
class gkGameObject;
class PathFindingAlgorithm;

/*! This is the Route class that can be used for calculation a route between waypoints */
class Route {

public:
	//! The constructor that needs a PathFindingAlgorithm pointer.
	/*!
	\param algorithm is a PathFindingAlgorithm pointer, this object calculates is used to calculate the path
	*/
	Route(PathFindingAlgorithm* algorithm);

	//! The destructor.
	/*!
	A more elaborate description of the destructor.
	*/
	virtual ~Route();

	//! Set the gkGameObject that must be followed
	/*!
	\param destination is an gkGameObject pointer.
	*/
	virtual void setDestination(gkGameObject* destination);

	//! Get the gkGameObject that is being followed
	/*!
	\param destination is an gkGameObject pointer.
	*/
	virtual gkGameObject* getDestination();

	//! Check if the supplied gkGameObject is at the destination waypoint
	/*!
	\param currentPosition is an gkGameObject pointer.
	\return returns true if the given object is at the destination waypoint
	*/
	virtual bool isAtDestination(gkGameObject* currentPosition);

	//! Check if the destination waypoint is change so the path must be recalculated.
	/*!
	\param newDestinationWayPoint is a WayPoint pointer that is changed to the new destination if the function result is true
	\return returns true if the destination waypoint is changed
	*/
	virtual bool isDestinationWayPointChanged(WayPoint* newDestinationWayPoint); 

	//! Calculates the next position if the destination is changed 
	/*!
	\param currentPosition is a gkGameObject pointer that represents the current location
	\return returns the location of the position where the object needs to move to.
	*/
	virtual gkGameObject* getHeading(gkGameObject* currentPosition);
private:
	WayPoint* _currentWayPoint;
	gkGameObject* _destination;
	WayPoint* _destinationWayPoint;
	PathFindingAlgorithm* _algorithm;
	stack<WayPoint*> _route;

	//! This function calculates the closest waypoint based on the current, next and prev waypoints. Only works if you can't skip waypoints.
	/*! 
	\return returns the closest WayPoint as a pointer
	*/
	WayPoint* _getClosestWayPointFromCurrentWayPoint(WayPoint* currentWayPoint, gkGameObject* currentLocation); 
};

