#pragma once

#include <stack>

using namespace std;

class WayPoint;

/* This is an interface that can be used for pathCalculation between waypoints */
class PathFindingAlgorithm {

public:
	virtual ~PathFindingAlgorithm() { }
	virtual void calculatePath(WayPoint* source, WayPoint* dest, stack<WayPoint*>& route) = 0;
};
