#pragma once

#include <string>
#include <vector>

using namespace std;

class StringHelper
{
public:
	static void split(vector<string> &tokens, const string &text, string sep);
};

