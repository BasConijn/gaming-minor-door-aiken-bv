#pragma once

#include "NPC.h"
#include "WayPoint.h"
#include "Route.h"
#include "Virus.h"
#include "PlayableCharacter.h"



class Level;

class RedBloodCell : public NPC {

public:
	RedBloodCell(PlayableCharacter* virus);
	virtual ~RedBloodCell();

	virtual RedBloodCell* clone(gkScene* dest);

	virtual void follow(gkGameObject* object);

	virtual void follow(Route* route);

	virtual void load(gkBlendFile* file, gkScene* dest);

	virtual void update(gkScalar delta);

	virtual void setWaypoint(WayPoint* waypoint);
	virtual WayPoint* nextWaypoint();

private:
	WayPoint* _currentWaypoint;
	PlayableCharacter* _player;
	
};
