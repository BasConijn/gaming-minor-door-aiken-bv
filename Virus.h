#pragma once

#include "PlayableCharacter.h"
#include "Application.h"

class Virus : public PlayableCharacter {

public:
	Virus();
	virtual ~Virus();

	virtual void load(gkBlendFile* file, gkScene* dest);

	virtual Virus* clone(Virus* virus);

	virtual void update(gkScalar delta);

private: 
	virtual void loadConstrains(gkScene* dest);

	virtual void loadAnimation();

	virtual void addStateTransitions();

	virtual bool isMoving();
	virtual bool isIdle();

	virtual void idleState();
	virtual void movingState();

	gkAnimations _animations;
};

