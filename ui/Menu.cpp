#include <gkLogger.h>
#include <gkScene.h>
#include <gkString.h>
#include <gkUtils.h>
#include <gkWindow.h>
#include <GUI/gkGUI.h>
#include <GUI/gkGUIManager.h>

#include <Loaders/Blender2/gkBlendFile.h>
#include <Loaders/Blender2/gkBlendLoader.h>

#include <Rocket/Controls.h>
#include <Rocket/Core/ReferenceCountable.h>
#include <Rocket/Controls/Lua/Controls.h>
#include <Rocket/Core/Lua/Interpreter.h>

#include "Menu.h"

#define MENU_BLENDER_FILE "assets/models/MainMenu.blend"
#define ROCKET_CURSOR_PAGE	"Cursor.rml"
#define ROCKET_DEMO_PAGE	"MainMenu.rml"

Menu::Menu() {

}

void Menu::load(gkString groupName) {
    gkBlendLoader::getSingleton().loadFile(gkUtils::getFile(MENU_BLENDER_FILE), "MainMenuScene", groupName);
}

void Menu::show(gkScene* scene) {
    GK_ASSERT(scene);

    // Install fonts
    gkGUIManager *gm = gkGUIManager::getSingletonPtr();
//    Rocket::Core::FontDatabase::LoadFontFace(Rocket::Core::String("assets/fonts/Delicious-Roman.otf"));
    gm->loadFont("assets/fonts/Delicious-Roman.otf");
    gm->loadFont("assets/fonts/Delicious-Bold.otf");
    gm->loadFont("assets/fonts/Delicious-Italic.otf");
    gm->loadFont("assets/fonts/Delicious-BoldItalic.otf");

    // Initialise the Lua interface
    Rocket::Core::Lua::Interpreter::Initialise();
    Rocket::Controls::Lua::RegisterTypes(Rocket::Core::Lua::Interpreter::GetLuaState());
    //Rocket::Core::Vector2f v; v.Normalise();


    // Create context
    gkGUI *gui = scene->getDisplayWindow()->getGUI();


    // Enable debugger (shift+~)
    gm->setDebug(gui);

    // Load the mouse cursor and release the caller's reference.
    Rocket::Core::ElementDocument* cursor = gui->getContext()->LoadMouseCursor(ROCKET_CURSOR_PAGE);
    if (cursor)
        cursor->RemoveReference();

    _document = gui->getContext()->LoadDocument(ROCKET_DEMO_PAGE);
    if (_document)
    {
//        Rocket::Core::Element* button = _document->GetElementById(DEMO_PAGE_BUTTON_ID);
//        if (button)
//            button->AddEventListener("click", eventListener);
        _document->Show();
    }
}
