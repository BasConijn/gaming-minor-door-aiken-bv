#ifndef MENU_H
#define MENU_H

#include <gkString.h>
#include <Rocket/Core/ElementDocument.h>

class gkScene;

class Menu
{
public:
    Menu();
    void load(gkString groupName);
    void show(gkScene *scene);
private:
    Rocket::Core::ElementDocument* _document;
};

#endif // MENU_H
