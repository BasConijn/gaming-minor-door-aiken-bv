#pragma once

#include "Input.h"
#include "gkGameObject.h"
#include "PlayableCharacter.h"

class KeyboardAndMouseInput : public Input {

public:
	KeyboardAndMouseInput(PlayableCharacter* player);
	virtual ~KeyboardAndMouseInput();

	virtual bool hasInput(const InputCode& ic);

	virtual void moveCamera(void);
	virtual void movePlayer(void);

	virtual void bindScanCode(const InputCode& ic, const int &code);

private:
	PlayableCharacter* _player;
	gkKeyboard*   _keyboard;
	gkMouse*      _mouse;
	int           _bindings[IC_MAX];
};

