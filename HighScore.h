#pragma once

#include <vector>

using namespace std;

class HighScoreElement;

class HighScore {

public:
	virtual ~HighScore();

	virtual void addHighScore(const HighScoreElement&);

	virtual vector<HighScoreElement> HighScore::getHighScore();

	virtual void readHighScoreFromFile();

	virtual void writeHighScoreToFile();

	static HighScore* getSingleton();

private:

	HighScore();
	HighScore(const HighScore&);

	vector<HighScoreElement> _highScoreElements;
};

