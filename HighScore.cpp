#include <iostream>
#include <fstream>
#include <algorithm>

#include "HighScore.h"
#include "HighScoreElement.h"

#define HIGHSCOREFILE "HighScore.score"

HighScore::HighScore()  {

}

HighScore::~HighScore() {

}

void HighScore::addHighScore(const HighScoreElement& highScoreElement)
{
	_highScoreElements.push_back(highScoreElement);
}

vector<HighScoreElement> HighScore::getHighScore() {
	std::sort(_highScoreElements.begin(), _highScoreElements.end(), std::greater<HighScoreElement>() );
	return _highScoreElements;
}

void HighScore::readHighScoreFromFile() {
	ifstream scoreFile(HIGHSCOREFILE);

	if(!scoreFile.is_open())
		return;

	_highScoreElements.clear();
	while (!scoreFile.eof()) 
	{
		string line;
		string playerName;
		int highScore;

		getline(scoreFile, line);

		if(line == "")
			continue;

		int i;
		for(i = line.size() - 1; i >=0; i--) {
			if(!isdigit(line[i])) {
				break;
			}
		}

		playerName = line.substr(0, i);
		highScore = atoi(line.substr(i, line.size()).c_str());

		HighScoreElement se(playerName, highScore);
		addHighScore(se);
	}
}

void HighScore::writeHighScoreToFile() {
	ofstream scoreFile (HIGHSCOREFILE);

	if(!scoreFile.is_open())
		return;

	vector<HighScoreElement>::iterator HighScoreElementIterator = _highScoreElements.begin();
	for(; HighScoreElementIterator != _highScoreElements.end(); HighScoreElementIterator++ ) {
		scoreFile << (*HighScoreElementIterator).getPlayerName() << " " << (*HighScoreElementIterator).getHighScore() << endl;	
	}
}

HighScore* HighScore::getSingleton()
{
	static HighScore *instance = new HighScore();

	return instance;
}
