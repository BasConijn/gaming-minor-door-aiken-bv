#include "PlayableCharacter.h"

PlayableCharacter::PlayableCharacter() : _input(0), _camera(0), _xRot(0), _zRot(0) {

}

PlayableCharacter::~PlayableCharacter() {

}

Input* PlayableCharacter::getInput() {
	return _input;
}

void PlayableCharacter::setInput(Input* input) {
	_input = input;
}

gkCamera* PlayableCharacter::getCamera() {
	return _camera;
}
void PlayableCharacter::setCamera(gkCamera* camera) {
	_camera = camera;
}

gkGameObject* PlayableCharacter::getXRot() {
	return _xRot;
}

void PlayableCharacter::setXRot(gkGameObject* xrot) {
	_xRot = xrot;
}

gkGameObject* PlayableCharacter::getZRot() {
	return _zRot;
}

void PlayableCharacter::setZRot(gkGameObject* zrot) {
	_zRot = zrot;
}

