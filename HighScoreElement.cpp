#include "HighScoreElement.h"

HighScoreElement::HighScoreElement(string playerName, int highScore) : _playerName(playerName), _highScore(highScore) {
	
}

HighScoreElement::~HighScoreElement() {

}

int HighScoreElement::getHighScore() const {
	return _highScore;
}

string HighScoreElement::getPlayerName() const {
	return _playerName;
}

bool operator<(const HighScoreElement& elm1, const HighScoreElement& elm2) {
    return elm1.getHighScore() < elm2.getHighScore();
}

bool operator>(const HighScoreElement& elm1, const HighScoreElement& elm2) {
    return elm1.getHighScore() > elm2.getHighScore();
}