#pragma once

#include "Application.h"
#include "WayPointManager.h"
#include "gkLogger.h"
#include "gkMemoryTest.h"
#include "gkUserDefs.h"
#include "OgreKit.h"
#include "HighScore.h"

int main(int argc, char** argv) {
	TestMemory;

	gkLogger::enable("AppCppDemo.log", GK_CPP_VERBOSE_LOG);

	gkUserDefs prefs;
	prefs.rendersystem = OGRE_RS_GL;
	prefs.winsize.x = 1360.f;
	prefs.winsize.y = 768.f;
	//prefs.fullscreen = true;
	prefs.wintitle = "Gaming & Simulation";
	prefs.verbose = GK_CPP_VERBOSE_LOG;
	//prefs.fsaa = true;
	//prefs.debugPhysics = true;
	prefs.debugFps = true;
	//prefs.grabInput = false;

	gkEngine eng(&prefs);

	//Setup everything. Ogre, gk etc.
	eng.initialize();

	if (!eng.isInitialized())
	{
		gkPrintf("Failed to initialize engine.\n");
		return 1;
	}

	{
		Application* application = Application::getSingleton();

		//Connect game tick callbacks
		eng.addListener(application);

		if (!eng.initializeStepLoop())
		{
			gkPrintf("Failed to initialize main loop.\n");
			return 1;
		}

		gkPrintf("Enter main loop.\n");

		while(1)
		{
			if (!eng.stepOneFrame())
			{
				gkPrintf("Exit main loop.\n");
				break;
			}
		}
	}

	delete Application::getSingleton();
	delete WayPointManager::getSingleton();
	delete HighScore::getSingleton();

	eng.finalize();
}