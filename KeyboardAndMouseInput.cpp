#include "KeyboardAndMouseInput.h"
#include "gkWindowSystem.h"
#include "gkGameObject.h"
#include "gkEntity.h"
#include "Application.h"
#include "gkLogger.h"

KeyboardAndMouseInput::KeyboardAndMouseInput(PlayableCharacter* player) {

	_player = player;
	_keyboard = gkWindowSystem::getSingleton().getKeyboard();
	_mouse = gkWindowSystem::getSingleton().getMouse();

	bindScanCode(IC_FORWARD,	KC_WKEY);
	bindScanCode(IC_BACKWARD,	KC_SKEY);
	bindScanCode(IC_LEFT,		KC_AKEY);
	bindScanCode(IC_RIGHT,		KC_DKEY);
	bindScanCode(IC_STOP,		KC_NONE);
}


KeyboardAndMouseInput::~KeyboardAndMouseInput() {

}

bool KeyboardAndMouseInput::hasInput(const InputCode& ic) {
	//switch (ic) {
	//case IC_FORWARD:
		return _keyboard->isKeyDown((gkScanCode)_bindings[ic]);
	//case IC_BUTTON_0:
	//case IC_BUTTON_2:
	//	return _mouse->isButtonDown(_bindings[ic]);
	//}
	//return false;
}

void KeyboardAndMouseInput::moveCamera() {

	if (_mouse->mouseMoved())
	{
		const gkScalar tick = gkAppData::gkFixedTickDelta * .25f;
		
		if (_mouse->relative.x!= 0.f)
			_player->getZRot()->roll(-gkRadian(_mouse->relative.x * tick));
		if (_mouse->relative.y!= 0.f)
			_player->getXRot()->pitch(-gkRadian(_mouse->relative.y * tick));
	}
}

void KeyboardAndMouseInput::movePlayer(void) {

	gkScalar speed = _player->getSpeed();
 
	if(_player->getXRot()->getOrientation() != gkEuler(0,0,0).toQuaternion() || _player->getZRot()->getOrientation() != gkEuler(0,0,0).toQuaternion())
	{
		//Calculate the local x and z axis
		gkVector3 localX = _player->getPhysics()->getOrientation() * gkVector3::UNIT_X;
		gkVector3 localZ = _player->getPhysics()->getOrientation() * gkVector3::UNIT_Z;

		//calculate the rotation to the camera
		gkQuaternion localRotationX(gkRadian(_player->getXRot()->getRotation().x), localX);
		gkQuaternion localRotationZ(gkRadian(_player->getZRot()->getRotation().z), localZ);

		//do the rotation
		_player->getPhysics()->rotate(localRotationX);
		_player->getPhysics()->rotate(localRotationZ);

		//reset the camera rotation
		_player->getXRot()->setOrientation(gkEuler(0,0,0));
		_player->getZRot()->setOrientation(gkEuler(0,0,0));
	}

	gkVector3 movement = _player->getPhysics()->getOrientation()  * gkVector3(0, speed, 0);
	_player->getPhysics()->setLinearVelocity(movement);
}

void KeyboardAndMouseInput::bindScanCode(const InputCode& ic, const int &code)
{
	int iic = (int)ic;
	if (iic >= 0 && iic < IC_MAX)
		_bindings[ic] = code;
}
