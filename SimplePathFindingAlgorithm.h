#pragma once

#include "PathFindingAlgorithm.h"

class SimplePathFindingAlgorithm : public PathFindingAlgorithm {

public:
	SimplePathFindingAlgorithm();
	virtual ~SimplePathFindingAlgorithm();
	virtual void calculatePath(WayPoint* source, WayPoint* dest, stack<WayPoint*>& route);
};
