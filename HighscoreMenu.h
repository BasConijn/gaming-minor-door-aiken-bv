#pragma once
#include "basemenu.h"

class HighscoreMenu :
	public BaseMenu
{
public:
	HighscoreMenu(gkScene* scene);

	virtual ~HighscoreMenu(void);

	virtual void loadMenu();

	virtual void unloadMenu();

	virtual void ProcessEvent(Rocket::Core::Event& event);

private:
	Rocket::Core::ElementDocument* _document;
	Rocket::Core::ElementDocument* _cursor;
	Rocket::Core::Element* _button;
};

