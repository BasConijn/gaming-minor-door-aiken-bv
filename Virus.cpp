#include "Level.h"
#include "Virus.h"
#include "Input.h"
#include "OgreKit.h"
#include "Application.h"
#include "gkLogger.h"

#define VIRUS_PHYSICS "ProtoVirus"

Virus::Virus() {

}

Virus::~Virus() {

}

void Virus::load(gkBlendFile* file, gkScene* dest) {
	gkScene* tscene = file->getMainScene();

	setSkeleton(tscene->getObject("ProtoVirusRig")->getSkeleton());
	setEntity(tscene->getObject("ProtoVirusMesh")->getEntity());
	setCamera(tscene->getObject("ProtoVirusCam")->getCamera());
	setZRot(tscene->getObject("zRot"));
	setXRot(tscene->getObject("xRot"));
	setPhysics(tscene->getObject("ProtoVirusPhysics"));

	dest->addObject(getSkeleton());
	dest->addObject(getEntity());
	dest->addObject(getCamera());
	dest->addObject(getZRot());
	dest->addObject(getXRot());
	dest->addObject(getPhysics());

	getCamera()->setMainCamera(true);

	loadConstrains(dest);
	loadAnimation();
	addStateTransitions();
	setSpeed(10.f);
}
void Virus::loadAnimation(){
	getEntity()->addAnimation("ProtoVirusMove");
	getEntity()->addAnimation("ProtoVirusIdle");
	_animations[ANIM_MOVING]   = getEntity()->getAnimationPlayer("ProtoVirusMove");
	_animations[ANIM_IDLE]   = getEntity()->getAnimationPlayer("ProtoVirusIdle");
}

Virus* Virus::clone(Virus* virus) {
	return virus;
}

void Virus::update(gkScalar delta) {
	Input* input = getInput();
	
	if(getCamera())
	{
		input->moveCamera();
	}

	gkFSM::update();

	switch(getState())
	{
		case PLAYER_IDLE:
			idleState();
			break;
		case PLAYER_MOVING:
			movingState();
			break;
	}
}

void Virus::loadConstrains(gkScene* dest) {
	
}

void Virus::addStateTransitions() {
	addTransition(PLAYER_IDLE, PLAYER_MOVING)->when(new gkFSM::LogicEvent<Virus>(this, &Virus::isMoving));
	addTransition(PLAYER_MOVING, PLAYER_IDLE)->when(new gkFSM::LogicEvent<Virus>(this, &Virus::isIdle));

	setState(PLAYER_IDLE, false);
}

bool Virus::isMoving() {
	return getInput()->hasInput(Input::IC_FORWARD);
}

bool Virus::isIdle() {
	return !getInput()->hasInput(Input::IC_FORWARD);
}

void Virus::idleState() {

	if(_animations[ANIM_MOVING]->isDone())
	{
		getSkeleton()->stopAnimation(_animations[ANIM_MOVING]);
		getSkeleton()->playAnimation(_animations[ANIM_IDLE], gkAppData::gkGlobalActionBlend);
	}

	gkVector3 movement = getPhysics()->getLinearVelocity();
	movement = movement * 0.95;
	getPhysics()->setLinearVelocity(movement);
}

void Virus::movingState() {
	getInput()->movePlayer();
	getSkeleton()->playAnimation(_animations[ANIM_MOVING], gkAppData::gkGlobalActionBlend);
	if(getSpeed() < 10)
	{
		float temp;
		temp = getSpeed() + 0.08;
		setSpeed(temp);
	}
}

