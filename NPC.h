#pragma once

#include "MoveableCharacter.h"

class Route;

class NPC : public MoveableCharacter {

public:
	NPC() { }

	virtual ~NPC() { }

	virtual void follow(gkGameObject* object) = 0;

	virtual void follow(Route* route) = 0;

	virtual void load(gkBlendFile* file, gkScene* dest) = 0;

	virtual void update(gkScalar delta) = 0;
};
