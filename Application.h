#pragma once

class gkGameLevel;
class gkGamePlayer;
class gkGameController;
class Menu;

#include "akMathUtils.h"
#include "ScoreCalculator.h"
#include "OgreKit.h"
#include "Level.h"

#define GK_CPP_VERBOSE_LOG false

class Application : public gkEngine::Listener {

public:
	virtual ~Application();
	virtual void loadHuds();
	virtual void resume();
	virtual void quit();
	virtual void gameOver();
	virtual void pause();
	virtual bool isPaused();
	virtual void tick(gkScalar delta);

	static Application* getSingleton();

private:
	Level* _level;
	gkHUD* _hudScore;
	gkHUDElement* _score;
	ScoreCalculator* _scoreCalculator;
	gkKeyboard* _keyboard;
	bool _paused;
	Application();
	Menu* _menu;
};

namespace gkAppData
{
static const gkScalar gkFixedTickDelta    = 1.f / 60.f;
static const gkScalar gkPlayerHeadZ       = 0.25f;
static const gkScalar gkCameraTol         = 0.25f;
static const gkScalar gkGlobalActionBlend = 35.98765f;
};

enum gkPlayerStates
{
	PLAYER_IDLE,
	PLAYER_MOVING
};

enum gkAnimationStates
{
	ANIM_IDLE,
	ANIM_MOVING,
	ANIM_MAX
};

class gkAnimationPlayer;
typedef gkAnimationPlayer* gkAnimations[ANIM_MAX];
