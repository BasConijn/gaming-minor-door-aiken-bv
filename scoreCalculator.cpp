#include "ScoreCalculator.h"
#include "OgreKit.h"
#include "Application.h"

ScoreCalculator::ScoreCalculator(void) : _startTime(0),_timeBeforePauze(0){
	timer = new Ogre::Timer();
}


ScoreCalculator::~ScoreCalculator(void){

}

void ScoreCalculator::start(){
	_startTime =timer->getMillisecondsCPU();
}

void ScoreCalculator::pause(){
	_timeBeforePauze += timer->getMillisecondsCPU() - _startTime;
}


long ScoreCalculator::calculateScore(){
	int currentTime = 0;
	if(!Application::getSingleton()->isPaused()) {
		currentTime = timer->getMillisecondsCPU() - _startTime;
	}
	return (currentTime + _timeBeforePauze)/1000;
}
