#include "OgreKit.h"
#include "MoveableCharacter.h"
#include "gkLogger.h" 

MoveableCharacter::MoveableCharacter() : _skeleton(0), _entity(0), _physics(0), _speed(0) {

}

void MoveableCharacter::lookAt(const gkVector3& position)
{
	gkVector3 front = position - getPhysics()->getWorldPosition();

	gkVector3 up    = gkVector3::UNIT_Z;
	gkVector3 right = front.crossProduct(up);

	up = right.crossProduct(front);

	front.normalise();
	up.normalise();   
	right.normalise();

	getPhysics()->setOrientation(gkQuaternion(right, front, up));

	//gkLogMessage("Look at: " << getPhysics()->getOrientation());
  }
	
gkSkeleton* MoveableCharacter::getSkeleton() {
	return _skeleton;
}

void MoveableCharacter::setSkeleton(gkSkeleton* skeleton) {
	_skeleton = skeleton;
}

gkGameObject* MoveableCharacter::getPhysics() {
	return _physics;
}

void MoveableCharacter::setPhysics(gkGameObject* physics) {
	_physics = physics;
}

gkEntity* MoveableCharacter::getEntity() {
	return _entity;
}

void MoveableCharacter::setEntity(gkEntity* entity) {
	_entity = entity;
}

gkScalar MoveableCharacter::getSpeed(){
	return _speed;
}

void MoveableCharacter::setSpeed(gkScalar speed){
	_speed = speed;
}

void MoveableCharacter::pause(){
	if(_physics) {		
		_physics->getPhysicsController()->suspend(true);
	}
		
	if(_skeleton) {
		_skeleton->pauseAnimations();
	}
}

void MoveableCharacter::resume(){
	if(_physics) {	
		_physics->getPhysicsController()->suspend(false);
	}

	if(_skeleton) {
		_skeleton->resumeAnimations();
	}
}