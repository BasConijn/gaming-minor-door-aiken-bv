#pragma once

#include "gkMathUtils.h"
#include <map>
#include <set>

using namespace std;

class gkBlenderFile;
class WayPoint;

class WayPointManager {

public:
	WayPointManager();
	virtual ~WayPointManager();

	static WayPointManager* getSingleton();

	virtual void load(gkBlendFile* file, gkScene* dest);

	virtual void update(gkScalar delta);

	virtual WayPoint* getClosestWayPoint(gkGameObject* object);

	virtual set<WayPoint*> getAllWayPoints();

	virtual WayPoint* getWayPointByName(string name);

private:
	map<string, WayPoint*> waypoints;

	WayPoint* _getWayPointAndCreateIfNotExists(string name, gkScene* scene);
};
