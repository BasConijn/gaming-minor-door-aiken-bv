#pragma once

#include "gkMathUtils.h"
#include <map>
#include <string>
#include <vector>

using namespace std;

class gkGameObject;

class WayPoint {

public:
	WayPoint(string name, gkGameObject* location);
	virtual ~WayPoint();

	virtual gkGameObject* getLocation();

	virtual string getName();

	virtual vector<WayPoint*> next();

	virtual vector<WayPoint*> previous();

	virtual void addNext(WayPoint* waypoint);

	virtual void addPrevious(WayPoint* waypoint);

	virtual void update(gkScalar delta);

	static string constructName(string baseName, int number);

private:
	string _name;
	gkGameObject* _location;
	map<WayPoint*, double> _next;
	map<WayPoint*, double> _previous;
	double _flow;
};
