#pragma once

#include "BaseMenu.h"

using namespace std;

class Menu : 
	  public BaseMenu
{

public:
	Menu(gkScene* scene);
	virtual ~Menu();

	virtual void loadMenu();

	void unloadMenu();

	virtual void ProcessEvent(Rocket::Core::Event& event);

private:
	Rocket::Core::ElementDocument* _document;
	Rocket::Core::ElementDocument* _cursor;

	enum {
		Play = 0,
		Highscore = 1,
		Settings = 2,
		Quit = 3
	};

	vector< pair<string, Rocket::Core::Element*>> _buttons;

	int _buttonToEnum(Rocket::Core::String name);
};

