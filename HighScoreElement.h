#pragma once

#include <string>

using namespace std;

class HighScoreElement {

public:
	HighScoreElement(string playerName, int highScore);
	virtual ~HighScoreElement();

	virtual int getHighScore() const;
	virtual string getPlayerName() const;

	friend bool operator<(const HighScoreElement& elm1, const HighScoreElement& elm2);
	friend bool operator>(const HighScoreElement& elm1, const HighScoreElement& elm2);

private:
	int _highScore;
	string _playerName;
};