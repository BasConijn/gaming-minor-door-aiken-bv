# Repository information #
This repository contains de software for the Gaming and Simulation minor that is given at the Hague University of Applied Science in Delft.
In here the game will be maintained by the group Aiken B.V. The game is will be built on gamekit.

# Group information #
The group contains 5 developers of which all are going to be involved in designing and developing the entire game.

* Fionn
* Bas
* Tom
* Aiken
* Rien

# Concept #
The game will be a kind of racing game that is located inside your body. You are a virus that will need to run away from the anti-bodies in the
blood. Levels can be inside of veins  and inside a petri dish. The following requirements have been given by the head teacher.

> The game must be made in gamekit and must be based on a medical environment.
> It needs to be made in a 3D environment and built on gamekit.
> There must be a element of simulation in the game and it must therefor be made as realistic as posible with proper physics.
D.R. Stikkolorum

This is the readme

example code:
```
function draw() {
	drawObject(x, y);
	calculatePhysics();

	fionn.getOnMyLevel();
}
```

