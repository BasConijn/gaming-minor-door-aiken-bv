#include "HighscoreMenu.h"
#include "HighScore.h"
#include "HighScoreElement.h"

HighscoreMenu::HighscoreMenu(gkScene* scene) : BaseMenu(scene), _document(0)
{
}


HighscoreMenu::~HighscoreMenu(void)
{
	unloadMenu();
}

void HighscoreMenu::loadMenu(){
	BaseMenu::loadMenu();

	gkGUI *gui = getScene()->getDisplayWindow()->getGUI();
	
	// Load the mouse cursor and release the caller's reference.
	_cursor = gui->getContext()->LoadMouseCursor("Cursor.rml");
	if (_cursor)
		_cursor->RemoveReference();

	_document = gui->getContext()->LoadDocument("Highscore.rml");

	if (_document) {
		
		_button = _document->GetElementById("Back");
		_button->AddEventListener("click", this);

		Rocket::Core::Element* highscoreDiv = _document->GetElementById("Highscore");
		HighScore* highscore = HighScore::getSingleton();

		highscore->readHighScoreFromFile();

		vector<HighScoreElement> highScoreElements = highscore->getHighScore();

		stringstream ss;
		for(int i =0; i < 10 && i < highScoreElements.size(); i++) {
			ss << highScoreElements[i].getPlayerName() << " " << highScoreElements[i].getHighScore() << "<br/>";	
		}
		highscoreDiv->SetInnerRML(ss.str().c_str());

		_document->Show();		
	}
}

void HighscoreMenu::unloadMenu(){
		getScene()->getDisplayWindow()->getGUI()->getContext()->UnloadAllMouseCursors();
		getScene()->getDisplayWindow()->getGUI()->getContext()->UnloadAllDocuments();
			
		_document->RemoveReference();
		_document = 0;

		Rocket::Core::Lua::Interpreter::Shutdown();

		_button->RemoveEventListener("click", this);
}

void HighscoreMenu::ProcessEvent(Rocket::Core::Event& event) {
		unloadMenu();

		getPrevious()->loadMenu();
}