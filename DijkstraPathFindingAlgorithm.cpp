#include "DijkstraPathFindingAlgorithm.h"
#include <map>
#include <vector>
#include "WayPointManager.h"
#include <set>
#include "WayPoint.h"
#include "OgreKit.h"

using namespace std;

//http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

DijkstraPathFindingAlgorithm::DijkstraPathFindingAlgorithm() {

}


DijkstraPathFindingAlgorithm::~DijkstraPathFindingAlgorithm() {

}

void DijkstraPathFindingAlgorithm::calculatePath(WayPoint* source, WayPoint* dest, stack<WayPoint*>& route) {

	//Empty the current route
	while(!route.empty()) {
		route.pop();
	}

	map<WayPoint*, double> distances;
	map<WayPoint*, WayPoint*> previousPath;

	set<WayPoint*> waypoints = WayPointManager::getSingleton()->getAllWayPoints();
	set<WayPoint*>::iterator iterator = waypoints.begin();

	//Set the distances to 0 and reset the path
	for(; iterator != waypoints.end(); iterator++) {
		distances[(*iterator)] = -1;
		previousPath[(*iterator)] = 0;
	}

	distances[source] = 0;

	//Loop through the waypoints
	while(!waypoints.empty()) {
		WayPoint* minDistanceWayPoint = 0;
		double minDistance;

		//Get the WayPoint that has the least distance from the source
		map<WayPoint*, double>::iterator distanceIterator = distances.begin();
		for(; distanceIterator != distances.end(); distanceIterator++) {
			
			if((*distanceIterator).second == -1) //Not infinite
				continue;

			if(waypoints.find((*distanceIterator).first) == waypoints.end()) //Not in waypoint set
				continue;

			if(minDistanceWayPoint == 0 || (*distanceIterator).second < minDistance) { //Distance is smaller
				minDistanceWayPoint = (*distanceIterator).first;
				minDistance = (*distanceIterator).second;
			}
		}
		
		WayPoint* u = minDistanceWayPoint;
		waypoints.erase(u); //Remove from used waypoints

		if(distances[u] == -1) {
			break;
		}

		//Add the neighbors into a set
		set<WayPoint*> neighbors;
		vector<WayPoint*> next = u->next();
		vector<WayPoint*> prev = u->previous();
		neighbors.insert(next.begin(), next.end());
		neighbors.insert(prev.begin(), prev.end());

		//Loop through the neighbors and calculate the distance
		//TODO: Store distances, because they don't change anyway
		set<WayPoint*>::iterator v = neighbors.begin();
		for(; v != neighbors.end(); v++) {
			double alt = distances[u] + u->getLocation()->getPosition().squaredDistance( (*v)->getLocation()->getPosition());

			if(alt < distances[(*v)] || distances[(*v)] == -1) {
				distances[(*v)] = alt;
				previousPath[(*v)] = u;
			}
		}
	}

	//Go in reverse and add the path to the stack.
	WayPoint* current = dest;
	while(current != source) {
		route.push(current);
		current = previousPath[current];
	}
	route.push(source);
}
