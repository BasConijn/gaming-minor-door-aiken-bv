#pragma once

#include "gkFSM.h"

class MoveableCharacter : public gkFSM {

public:

	MoveableCharacter();

	virtual ~MoveableCharacter() { }

	virtual void lookAt(const gkVector3& position);

	virtual void load(gkBlendFile* file, gkScene* dest) = 0;

	virtual void update(gkScalar delta) = 0;

	virtual gkSkeleton* getSkeleton();

	virtual void setSkeleton(gkSkeleton* skeleton);

	virtual gkGameObject* getPhysics();

	virtual void setPhysics(gkGameObject* physics);

	virtual gkEntity* getEntity();

	virtual void setEntity(gkEntity* entity);

	virtual gkScalar getSpeed();
	virtual void setSpeed(gkScalar speed);

	virtual void pause();

	virtual void resume();

private:
	gkSkeleton* _skeleton;
	gkGameObject* _physics;
	gkEntity* _entity;
	gkScalar _speed;
};
